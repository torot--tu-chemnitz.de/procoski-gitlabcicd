def sum(arg):
    """ Calculate the sum!"""
    total=0
    for val in arg:
        total += val
    return total

print('The sum of 1,2,3 is:', sum([1,2,3]))
